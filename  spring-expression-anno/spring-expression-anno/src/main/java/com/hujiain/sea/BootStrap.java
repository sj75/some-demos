package com.hujiain.sea;


import com.hujiain.sea.aonnation.EnableOperator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * start
 *
 * @author hujiabin
 * @date 2023/11/2 22:52
 * @since 1.0
 */
@SpringBootApplication
@EnableOperator
public class BootStrap {
    public static void main(String[] args) {
        SpringApplication.run(BootStrap.class, args);
    }
}
