package com.hujiain.sea.aonnation;

import com.hujiain.sea.register.OperatorComponentRegistrar;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * EnableOperator
 *
 * @author hujiabin
 * @date 2023/11/2 09:52
 * @since 1.0
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import(OperatorComponentRegistrar.class)
public @interface EnableOperator {
    /**
     * scanner path, default package root path
     */
    String[] value() default {};
}
