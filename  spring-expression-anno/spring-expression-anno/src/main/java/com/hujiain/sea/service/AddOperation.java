package com.hujiain.sea.service;

import com.hujiain.sea.aonnation.Operator;

/**
 * test add
 *
 * @author hujiabin
 * @date 2023/11/2 22:49
 * @since 1.0
 */
@Operator("#a + #b")
public interface AddOperation {
    Object add(int a, int b);
}
