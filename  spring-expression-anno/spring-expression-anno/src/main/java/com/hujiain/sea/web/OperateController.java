package com.hujiain.sea.web;

import com.hujiain.sea.service.AddOperation;
import com.hujiain.sea.service.SubOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * test api
 *
 * @author hujiabin
 * @date 2023/11/2 22:51
 * @since 1.0
 */
@RestController
public class OperateController {
    @Resource
    private AddOperation addOperation;
    @Resource
    private SubOperation subOperation;

    @GetMapping("add")
    public Object add() {
        return addOperation.add(1, 2);
    }

    @GetMapping("sub")
    public Object sub() {
        return subOperation.sub(5, 1);
    }
}
