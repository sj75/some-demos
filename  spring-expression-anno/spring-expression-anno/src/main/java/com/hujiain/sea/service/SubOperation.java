package com.hujiain.sea.service;

import com.hujiain.sea.aonnation.Operator;

/**
 * test sub
 *
 * @author hujiabin
 * @date 2023/11/2 22:50
 * @since 1.0
 */
@Operator("#a - #b")
public interface SubOperation {
    Object sub(int a, int b);
}
