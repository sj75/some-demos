package com.hujiain.sea.aonnation;

import java.lang.annotation.*;

/**
 * Operator
 *
 * @author hujiabin
 * @date 2023/11/2 09:19
 * @since 1.0
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface Operator {
    /**
     * Spel expression
     */
    String value();
}
